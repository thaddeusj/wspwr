import com.thogan.wspwr.MessageExecutor
import org.easymock.Capture
import org.jboss.netty.channel.{ChannelFuture, Channel}
import org.junit.Test
import org.junit.Assert._
import org.easymock.EasyMock._

class LDAPAuthTest {

    @Test
    def failedLoginTest() = {

        val chF: ChannelFuture = createMock(classOf[ChannelFuture])
        replay(chF)

        val ch: Channel = createMock(classOf[Channel])
        val respCap: Capture[String] = new Capture[String]
        expect(ch.write(capture(respCap))).andReturn(chF)
        replay(ch)

        val msg =
            """
              |{
              | "message" : "authenticate",
              | "body" : {
              |     "uidOrEmail" : "poop",
              |     "password" : "poopypass"
              | }
              |}
            """.stripMargin

        MessageExecutor.routeAndExec(ch, msg)
        Thread.sleep(2000)
        println(respCap.getValue)
    }

    @Test
    def successfulUIDLoginTest() = {
        val chF: ChannelFuture = createMock(classOf[ChannelFuture])
        replay(chF)

        val ch: Channel = createMock(classOf[Channel])
        val respCap: Capture[String] = new Capture[String]
        expect(ch.write(capture(respCap))).andReturn(chF)
        replay(ch)

        val msg =
            """
              |{
              | "message" : "authenticate",
              | "body" : {
              |     "uidOrEmail" : "testuser",
              |     "password" : "WsPwrIsTheAnswer"
              | }
              |}
            """.stripMargin

        MessageExecutor.routeAndExec(ch, msg)
        Thread.sleep(2000)
        println(respCap.getValue)
    }
    // "WsPwrIsTheAnswer"
}
