package com.thogan.wspwr

import net.liftweb.json._
import org.junit.Test
import org.junit.Assert._

case class TestCreds(user: String, pass: String)

class JSONTest {

    @Test
    def decodeTest() = {
        val obj = parse(""" { "message" : "authenticate", "Creds" : { "user" : "poop", "pass" : "poopypass" } } """)
        assertTrue((obj \ "message") == JString("authenticate"))

        val message = (obj \ "message") match {
            case JString(s) => assertTrue(s.equalsIgnoreCase("authenticate"))
            case _ => fail("Could not get string from parsed object")
        }

        implicit val formats = DefaultFormats
        val creds = (obj \ "Creds").extract[TestCreds]
        assertTrue(creds.user.equals("poop"))
        assertTrue(creds.pass.equals("poopypass"))
        println(creds)
    }
}
