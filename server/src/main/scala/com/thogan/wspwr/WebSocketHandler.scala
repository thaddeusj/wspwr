package com.thogan.wspwr

import org.jboss.netty.channel._
import org.jboss.netty.handler.codec.http._
import websocketx._
import org.jboss.netty.buffer.ChannelBuffers
import org.jboss.netty.util.CharsetUtil
import java.util.concurrent.atomic.AtomicLong

object IDGen {
    val seq = new AtomicLong(1)
    def next() = seq.getAndIncrement
}

class WebSocketHandler extends SimpleChannelUpstreamHandler {
    val WEBSOCKET_PATH = "/websocket"
    var handshaker: WebSocketServerHandshaker = _

    override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
        e.getMessage match {
            case req: HttpRequest => handleHttpRequest(ctx, req)
            case frame: WebSocketFrame => handleWebSocketFrame(ctx, frame)
        }
    }

    def handleHttpRequest(ctx: ChannelHandlerContext, req: HttpRequest) = {
        if (req.getUri.equals(WEBSOCKET_PATH) && req.getMethod == HttpMethod.GET) {
            val wsFactory = new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, false)
            handshaker = wsFactory.newHandshaker(req)
            if (handshaker != null) handshaker.handshake(ctx.getChannel, req).addListener(WebSocketServerHandshaker.HANDSHAKE_LISTENER)
        } else {
            sendHttpReponse(ctx, req, new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FORBIDDEN))
        }
    }

    def handleWebSocketFrame(ctx: ChannelHandlerContext, frame: WebSocketFrame) = {
        frame match {
            case cf: CloseWebSocketFrame => handshaker.close(ctx.getChannel, cf)
            case pf: PingWebSocketFrame => ctx.getChannel.write(new PongWebSocketFrame(pf.getBinaryData))
            case tf: TextWebSocketFrame => MessageExecutor.routeAndExec(ctx.getChannel, tf.getText)
            case _ => throw new UnsupportedOperationException("Unsupported WebSocket Frame")
        }
    }

    def sendHttpReponse(ctx: ChannelHandlerContext, req: HttpRequest, resp: HttpResponse) = {
        if (resp.getStatus().getCode != 200) {
            resp.setContent(ChannelBuffers.copiedBuffer(resp.getStatus.toString, CharsetUtil.UTF_8))
            HttpHeaders.setContentLength(resp, resp.getContent.readableBytes)
        }

        val cf = ctx.getChannel.write(resp)
        if (!HttpHeaders.isKeepAlive(req) || resp.getStatus.getCode != 200)
            cf.addListener(ChannelFutureListener.CLOSE)
    }

    override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
        e.getCause.printStackTrace()
        e.getChannel.close()
    }

    def getWebSocketURL(req: HttpRequest): String = "ws://" + HttpHeaders.getHost(req) + WEBSOCKET_PATH

    override def channelOpen(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
        super.channelOpen(ctx, e)
        println("Connection from: " + ctx.getChannel.getRemoteAddress.toString)
    }
}
