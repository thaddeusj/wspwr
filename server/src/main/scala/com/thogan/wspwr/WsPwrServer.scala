package com.thogan.wspwr

import org.jboss.netty.channel.{Channels, ChannelPipeline, ChannelPipelineFactory}
import org.jboss.netty.handler.codec.http.{HttpResponseEncoder, HttpRequestEncoder, HttpChunkAggregator, HttpRequestDecoder}
import org.jboss.netty.bootstrap.ServerBootstrap
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory
import java.util.concurrent.Executors
import java.net.InetSocketAddress
import java.util.Properties
import java.io.{FileReader, File}

class WebSocketPipelineFactory extends ChannelPipelineFactory {
    def getPipeline: ChannelPipeline = {
        val pipeline = Channels.pipeline()
        pipeline addLast("decoder", new HttpRequestDecoder)
        pipeline addLast("aggregator", new HttpChunkAggregator(1048576))
        pipeline addLast("encoder", new HttpResponseEncoder)
        pipeline addLast("handler", new WebSocketHandler)
        pipeline
    }
}

object WsPwrServer {
    def main(args: Array[String]): Unit = {
        val confFilePath = System.getProperty("wspwr.config")
        if (confFilePath == null) {
            println("System property wspwr.config must be specified")
            System.exit(1)
        }

        val confFile = new File(confFilePath)
        if (confFile.canRead == false) {
            println("Cannot read specified configuration file: " + confFilePath)
            System.exit(1)
        }

        val config = new Properties()
        var reader: FileReader = null

        try {
            reader = new FileReader(confFile)
            config.load(reader)
        } catch {
            case e: Exception => {
                e.printStackTrace()
                println("\nError reading configuration file: " + confFilePath)
                System.exit(1)
            }
        } finally {
            reader.close()
        }

        PasswordResetManager.config = config
        val listenPort = config.getProperty("listenPort", "8081").toInt

        println("Starting WebSocket Server on port " + listenPort + " ...")

        val bs = new ServerBootstrap(new NioServerSocketChannelFactory(
            Executors.newCachedThreadPool, Executors.newCachedThreadPool))
        bs setPipelineFactory(new WebSocketPipelineFactory)
        bs bind(new InetSocketAddress(listenPort))

        println("Server Started.")
    }
}
