package com.thogan.wspwr.telephony

import org.jboss.netty.handler.codec.frame.FrameDecoder
import org.jboss.netty.channel.{Channel, ChannelHandlerContext}
import org.jboss.netty.buffer.{ChannelBuffers, ChannelBuffer}
import collection.mutable
import org.jboss.netty.util.CharsetUtil

class FreeswitchMessageDecoder extends FrameDecoder {
    def decode(ctx: ChannelHandlerContext, ch: Channel, buf: ChannelBuffer): AnyRef = {
        if (buf.readableBytes >= 2) {
            decodeBuf(buf) match {
                case Some(msg) => msg
                case None => null
            }
        } else null
    }

    def decodeBuf(implicit buf: ChannelBuffer): Option[FreeswitchMessage] = {
        buf.markReaderIndex()

        findEndOfHeaders match {
            case Some(eoh) => {
                val headers = extractHeaders(eoh - buf.readerIndex)
                headers.get("Content-Length") match {
                    case Some(contentLength) => {
                        extractData(contentLength.toInt) match {
                            case Some(data) => Some(FreeswitchMessage(headers, data))
                            case None => { buf.resetReaderIndex(); None }
                        }
                    }
                    case None => Some(FreeswitchMessage(headers, ""))
                }
            }
            case None => None
        }
    }

    def findEndOfHeaders(implicit buf: ChannelBuffer): Option[Int] = {
        (for (
            i <- buf.readerIndex until (buf.readerIndex + buf.readableBytes - 1)
                if buf.getByte(i) == '\n' && buf.getByte(i + 1) == '\n'
        ) yield (i)).headOption
    }

    def extractHeaders(headersLen: Int)(implicit buf: ChannelBuffer): Map[String, String] = {
        val headers = new mutable.HashMap[String, String]

        buf.readBytes(headersLen).toString(CharsetUtil.UTF_8) split("\\n") foreach { p =>
            val h = p split(": ", 2)
            headers(h(0)) = h(1)
        }

        buf.skipBytes(2)
        headers.toMap
    }

    def extractData(length: Int)(implicit buf: ChannelBuffer): Option[String] = {
        if (buf.readableBytes() >= length) {
            Some(buf.readBytes(length).toString(CharsetUtil.UTF_8))
        } else None
    }
}
