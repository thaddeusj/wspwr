package com.thogan.wspwr.telephony

import org.jboss.netty.bootstrap.ClientBootstrap
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory
import java.util.concurrent.Executors
import org.jboss.netty.channel.Channel
import java.net.InetSocketAddress
import java.io.IOException

object ClientState extends Enumeration {
    type ClientState = Value
    val PreAuth = Value("PreAuth")
    val AwaitingAuthResp = Value("AwaitingAuthResp")
    val Ready = Value("Ready")
    val AwaitingOriginateResp = Value("AwaitingOriginateResp")
    val AwaitingEventLockResp = Value("AwaitingEventLockResp")
    val ReceivingEvents = Value("ReceivingEvents")
}

abstract class FreeswitchClient(host: String, port: Int, password: String) {
    var state = ClientState.PreAuth

    val bootstrap = new ClientBootstrap(
        new NioClientSocketChannelFactory(
            Executors.newCachedThreadPool, Executors.newCachedThreadPool))

    bootstrap.setPipelineFactory(new FreeswitchClientPipelineFactory(this))
    var ch: Channel = _

    var callUuid: String = _

    def connect() {
        val future = bootstrap.connect(new InetSocketAddress(host, port))
        ch = future.awaitUninterruptibly().getChannel
        if (future.isSuccess == false) {
            future.getCause.printStackTrace()
            bootstrap.releaseExternalResources()
            throw new IOException("Failed to connect to Freeswitch at " + host + ":" + port, future.getCause)
        }

        println("Connected to Flowroute at " + host + ":" + port)
    }

    def close() {
        ch.close()
    }

    def onReady()

    def onOtherState(msg: FreeswitchMessage) {
        throw new IOException("Client in a bad state.")
    }

    def originate(aLeg: String, bLeg: String, ignoreEarlyMedia: Boolean) {
        val cmdBuf = new StringBuilder
        cmdBuf.append("api originate ")
        if (ignoreEarlyMedia) cmdBuf.append("{ignore_early_media=true}")
        cmdBuf.append(aLeg)
        cmdBuf.append(" ")
        cmdBuf.append(bLeg)

        state = ClientState.AwaitingOriginateResp
        send(cmdBuf.toString())
        println("Freeswitch Client: Sent Originate " + aLeg + " to " + bLeg)
    }

    def transfer(dest: String) {
        println("Transferring " + callUuid + " to extension " + dest)
        send("api uuid_transfer " + callUuid + " " + dest)
    }

    def handleMessage(msg: FreeswitchMessage) {
        val origState = state

        state match {
            case ClientState.PreAuth => handleAuthReq(msg)
            case ClientState.AwaitingAuthResp => handleAuthResp(msg)
            case ClientState.AwaitingOriginateResp => handleOriginateResp(msg)
            case ClientState.AwaitingEventLockResp => handleEventLockResp(msg)
            case _ => onOtherState(msg)
        }

        if (state == ClientState.Ready) onReady()
    }

    def handleAuthReq(msg: FreeswitchMessage) {
        if (msg.headers("Content-Type") == "auth/request") {
            state = ClientState.AwaitingAuthResp
            send("auth " + password)
        }
    }

    def handleAuthResp(msg: FreeswitchMessage) {
        if (msg.headers("Content-Type") == "command/reply" &&
            msg.headers("Reply-Text").startsWith("+OK")) {
            state = ClientState.Ready
            println("Flowroute Client Authenticated")
        }
    }

    def handleOriginateResp(msg: FreeswitchMessage) {
        val resp = msg.data.stripLineEnd.split(" ")
        if (resp(0).equals("+OK")) callUuid = resp(1)
        state = ClientState.AwaitingEventLockResp
        println("sending: " + "myevents " + callUuid)
        send("myevents " + callUuid)
    }

    def handleEventLockResp(msg: FreeswitchMessage) {
        if (msg.headers("Reply-Text").startsWith("+OK")) {
            println("Locked onto events for call " + callUuid)
            state = ClientState.ReceivingEvents
        } else throw new IOException("Event lock not accepted for call " + callUuid)
    }

    def send(data: String) = ch.write(data + "\n\n")
}
