package com.thogan.wspwr.telephony

import org.jboss.netty.channel.{MessageEvent, ExceptionEvent, ChannelHandlerContext, SimpleChannelUpstreamHandler}

class FreeswitchClientHandler(client: FreeswitchClient) extends SimpleChannelUpstreamHandler {
    override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
        e.getMessage match {
            case msg: FreeswitchMessage => handleMessage(ctx, msg)
            case _ => println("Frame arrived that was not a FreeswitchMessage. Was a " + e.getMessage.getClass.toString)
        }
    }

    override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
        e.getCause.printStackTrace()
        ctx.getChannel.close()
    }

    def handleMessage(ctx: ChannelHandlerContext, msg: FreeswitchMessage) {
        client.handleMessage(msg)
    }
}
