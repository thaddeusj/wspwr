package com.thogan.wspwr.messages

import com.unboundid.ldap.sdk._
import scala.Some
import com.thogan.wspwr.{WsPwrMessage, PasswordResetManager, WsPwrMessageHandler}
import com.thogan.wspwr.PasswordResetManager._
import scala.Some

case class NewPassword(resetToken: String, password: String, confirmPassword: String)
case class SetPasswordResult(success: Boolean, canRetry: Boolean, msg: String)

class SetPassMessageHandler extends WsPwrMessageHandler {
    def run() {
        val conn = new LDAPConnection

        try {
            val newPass = body.extract[NewPassword]

            if (newPass.password.equals(newPass.confirmPassword)) {
                conn connect(ldapHost, ldapPort)
                conn bind(resetterDn, resetterPassword)

                // Acquire DN using reset token and set new password
                PasswordResetManager.resetTokens.consume(newPass.resetToken) match {
                    case Some(dn) => {
                        conn modify(new ModifyRequest(dn, new Modification(
                            ModificationType.REPLACE, "userPassword", newPass.password)))
                        sendSetResult(true, false, "Password reset complete.")
                    }
                    case None => sendSetResult(false, false, "Password reset session expired.")
                }
            } else sendSetResult(false, true, "Passwords did not match.")
        } catch {
            case lde: LDAPException => {
                lde.printStackTrace()
                sendSetResult(false, false, "Error connecting to LDAP backend.")
            }
            case e: Exception => sendSetResult(false, false, "Unknown backend error.")
        } finally {
            conn.close()
        }
    }

    def sendSetResult(success: Boolean, canRetry: Boolean, msg: String) =
        sendResult(WsPwrMessage("setPasswordResult", SetPasswordResult(success, canRetry ,msg)))
}
