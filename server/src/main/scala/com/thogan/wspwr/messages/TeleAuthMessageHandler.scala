package com.thogan.wspwr.messages

import com.thogan.wspwr.{PasswordResetManager, WsPwrMessage, WsPwrMessageHandler}
import com.unboundid.ldap.sdk._
import com.thogan.wspwr.PasswordResetManager._
import com.thogan.wspwr.telephony.FreeswitchVerifyDigits
import java.util.UUID

case class TeleAuthMessage(uidOrEmail: String, digits: String)
case class DigitEvent(digit: String)

class TeleAuthMessageHandler extends WsPwrMessageHandler {
    val fsHost = "goltel.goliath.thogan.lan"
    val fsPort = 8021
    val fsPassword = "tjh965"

    var dn: String = _

    def run() {
        val conn = new LDAPConnection()

        try {
            val teleAuth = body.extract[TeleAuthMessage]

            val searchField = if (teleAuth.uidOrEmail.contains("@")) "mail" else "uid"
            conn connect(ldapHost, ldapPort)

            val userSearchRes = conn.search(ldapBase, SearchScope.SUB,
                Filter.createEqualityFilter(searchField, teleAuth.uidOrEmail), "mobile")

            userSearchRes.getEntryCount match {
                case 1 => {
                    dn = userSearchRes.getSearchEntries.get(0).getDN
                    val mobile = userSearchRes.getSearchEntries.get(0).getAttributeValue("mobile")
                    println("Found mobile number " + mobile + " for DN " + dn)

                    val dest = "sofia/external/34378627#1" + mobile + "@sip.flowroute.com"
                    val verifier = new FreeswitchVerifyDigits(fsHost, fsPort, fsPassword, dest, teleAuth.digits, onComplete, onDigit)
                    verifier.connect()
                    Thread.sleep(40000)
                }
                case _ => sendAuthResult(false, "Invalid UID or Email")
            }

        } catch {
            case lde: LDAPException => sendAuthResult(false, "Error connecting to LDAP backend"); lde.printStackTrace()
            case e: Exception => sendAuthResult(false, "Unknown backend error"); e.printStackTrace()
        } finally {
            conn close()
        }
    }

    def sendAuthResult(success: Boolean, msg: String) = sendResult(WsPwrMessage("authResult", AuthResult(success, msg)))

    def onDigit(digit: Char): Unit = sendResult(WsPwrMessage("digitEvent", DigitEvent(digit.toString)))
    def onComplete(verified: Boolean): Unit = {
        if (verified) {
            val resetToken = UUID.randomUUID().toString
            PasswordResetManager.resetTokens.put(resetToken, dn, 120000)
            sendAuthResult(true, resetToken)
        } else sendAuthResult(false, "Incorrect verification code entered.")
    }
}
