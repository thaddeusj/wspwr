package com.thogan.wspwr.messages

import com.unboundid.ldap.sdk._
import java.util.UUID
import com.thogan.wspwr.{WsPwrMessage, PasswordResetManager, WsPwrMessageHandler}
import com.thogan.wspwr.PasswordResetManager._

case class Creds(uidOrEmail: String, password: String)

class AuthMessageHandler extends WsPwrMessageHandler {

    def run() {
        val conn = new LDAPConnection

        try {
            val creds = body.extract[Creds]

            val searchField = if (creds.uidOrEmail.contains("@")) "mail" else "uid"
            conn connect(ldapHost, ldapPort)

            val userSearchRes = conn.search(ldapBase, SearchScope.SUB,
                Filter.createEqualityFilter(searchField, creds.uidOrEmail), null)

            userSearchRes.getEntryCount match {
                case 1 => {
                    val dn = userSearchRes.getSearchEntries.get(0).getDN
                    authUser(conn, dn, creds.password) match {
                        case true => {
                            val resetToken = UUID.randomUUID().toString
                            PasswordResetManager.resetTokens.put(resetToken, dn, 120000)
                            sendAuthResult(true, resetToken)
                        }
                        case _ => sendAuthResult(false, "Invalid Credentials")
                    }
                }
                case _ => sendAuthResult(false, "Invalid Credentials")
            }

        } catch {
            case lde: LDAPException => sendAuthResult(false, "Error connecting to LDAP backend"); lde.printStackTrace()
            case e: Exception => sendAuthResult(false, "Unknown backend error"); e.printStackTrace()
        } finally {
            conn close()
        }
    }

    def authUser(conn: LDAPConnection, dn: String, password: String): Boolean = try {
        (conn bind(dn, password)).getResultCode == ResultCode.SUCCESS
    } catch {
        case lde: LDAPException => false
    }

    def sendAuthResult(success: Boolean, msg: String) = sendResult(WsPwrMessage("authResult", AuthResult(success, msg)))
}
