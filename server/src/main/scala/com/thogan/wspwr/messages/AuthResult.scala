package com.thogan.wspwr.messages

case class AuthResult(success: Boolean, msg: String)
