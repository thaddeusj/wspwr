package com.thogan.wspwr

import scala.collection.JavaConversions._
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import collection.mutable.ArrayBuffer

/**
 * Created with IntelliJ IDEA.
 * User: thogan
 * Date: 2/24/13
 * Time: 8:55 PM
 * To change this template use File | Settings | File Templates.
 */

case class ExpiringItem[V](item: V, expires: Long)

class ExpiringConcurrentHashMap[K, V](val interval: Long) {
    val data = new ConcurrentHashMap[K, ExpiringItem[V]]
    val reaper = new Reaper[K](data)

    reaper.start()

    def put(key: K, value: V, expiryMillis: Long) = {
        data.put(key, ExpiringItem(value, System.currentTimeMillis() + expiryMillis))
    }

    def consume(key: K): Option[V] = {
        data remove(key) match {
            case item: ExpiringItem[V] => item.expires match {
                case ex: Long if ex > System.currentTimeMillis() => Some(item.item)
                case _ => None
            }
            case _ => None
        }
    }

    def stopReaper() = {
        reaper.doRun.set(false)
        reaper.interrupt()
    }

    class Reaper[K](data: ConcurrentHashMap[K, _]) extends Thread {
        val doRun = new AtomicBoolean(true)

        override def run() {
            val expiredKeys = new ArrayBuffer[K]

            while (doRun.get) {
                data.keySet() map { k =>
                    data.get(k) match {
                        case ExpiringItem(_, expires) if expires <= System.currentTimeMillis() => expiredKeys += k
                        case _ =>
                    }
                }

                expiredKeys map { k => data.remove(k); println("Reaper expired key " + k.toString) }
                expiredKeys.clear()

                Thread.sleep(interval)
            }
        }
    }
}